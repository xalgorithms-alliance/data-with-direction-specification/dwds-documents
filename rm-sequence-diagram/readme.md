![rm sequence diagram](./RM_sequence-diagram_2020-12-17.png)

Source: Potvin, J. et. al. (Forthcoming, 2021). Data With Direction. Article in partial fulfillment of a Doctorate in Administration (Project Management). Université du Québec. Canada.
Contact: [jpotvin@xalgorithms.org](jpotvin@xalgorithms.org)``
[https://gitlab.com/xalgorithms-alliance](https://gitlab.com/xalgorithms-alliance)
[https://xalgorithms.org/](https://gitlab.com/xalgorithms-alliance)

Licensing:
Text and Images: CC-BY-SA 4.0 International
End-user applications: Apache 2.0
Network service components: GNU Affero GPL 3.0
(or later versions of those same licenses)
